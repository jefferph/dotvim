Installation
============

Clone the repo
--------------
    git clone https://jefferph@bitbucket.org/jefferph/dotvim.git ~/.vim

Get the submodules
------------------
    cd ~/.vim
    git submodule init
    git submodule update

Make some symlinks
------------------
    ln -s ~/.vim/vimrc ~/.vimrc
    ln -s ~/.vim/bundle/pathogen/autoload ~/.vim/autoload

