call pathogen#infect()
call pathogen#helptags()

colorscheme molokai

"switch on syntax highlighting
syn on

"minimum number of lines above/below cursor
set scrolloff=4

"new lines take indentation of current line
set autoindent
set smartindent
"number of spaces per (auto)indent
set shiftwidth=4

"number of spaces per tab
set tabstop=4
"convert tabs to spaces
set expandtab

"highlight search term
set hlsearch
"incremental search
set incsearch

highlight ExtraWhitespace ctermbg=darkgreen ctermfg=black guibg=darkgreen guifg=black
call matchadd('ExtraWhitespace', '\s\+$')

highlight ColorColumn ctermbg=235 guibg=#2c2d27
set colorcolumn=80

"use <F2> to toggle autoindent for pasting
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

nmap <F8> :TagbarToggle<CR>
